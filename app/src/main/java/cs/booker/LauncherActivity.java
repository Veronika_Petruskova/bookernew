package cs.booker;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LauncherActivity extends Activity {

    Button potvrd;
    EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        potvrd = (Button) findViewById(R.id.button2);
        email = (EditText) findViewById(R.id.enterEmail);
        //email.setText("");
        potvrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!email.getText().toString().equals("")) {
                    Intent intent = new Intent(LauncherActivity.this, ReservationsActivity.class);
                    intent.putExtra(MainActivity.USER_EMAIL, email.getText().toString());
                    startActivity(intent);
                }
                else Toast.makeText(LauncherActivity.this, "Vložte email", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
