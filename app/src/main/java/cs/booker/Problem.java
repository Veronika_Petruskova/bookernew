package cs.booker;

/**
 * Created by PaFi on 29. 3. 2016.
 */
public class Problem {
    private boolean isSelected=false;
    private String problem;
    private int uradId;

    public Problem(String problem, int id){
        this.setProblem(problem);
        this.setUradId(id);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    @Override
    public String toString() {
        return this.problem;
    }

    public int getUradId() {
        return uradId;
    }

    public void setUradId(int uradId) {
        this.uradId = uradId;
    }
}
