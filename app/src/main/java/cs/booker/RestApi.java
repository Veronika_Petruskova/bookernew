package cs.booker;

import java.util.List;

import cs.booker.RestModel.Odpoved;
import cs.booker.RestModel.Objednavka;
import cs.booker.RestModel.Pouzivatel;
import cs.booker.RestModel.Urad;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by Veronika on 26. 4. 2016.
 */
public interface RestApi {

    @GET("contacts")
    Call<List<Pouzivatel>> loadContacts();

    @GET("offices")
    Call<List<Urad>> loadOffices();

    @POST("reservations")
    Call<Odpoved> createReservation(@Body Objednavka obj);
}
