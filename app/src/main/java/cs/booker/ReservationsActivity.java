package cs.booker;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import cs.booker.RestModel.Urad;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ReservationsActivity extends Activity {
    ListViewAdapter adapter;
    ArrayList<Reservation> myReservations = new ArrayList<>();
    ListView listView;
    TextView noRes;
    String request, email;
    int pocetObj;
    RestApi myApi;
    SQLiteDatabase mydatabase;
    public static final String DATABASE_NAME = "Databaza";
    public static final String TABLE_NAME = "MojeObjednavky";
    public static final String TOKEN = "Token";
    public static final String DATETIME = "DateTime";
    public static final String URAD_ID = "UradId";
    public static final String STATE = "Stav";

    Retrofit retrofit;
    public int freeId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations);
        final Intent intent=getIntent();
        request=intent.getStringExtra(MainActivity.USER_REQUEST);
        email=intent.getStringExtra(MainActivity.USER_EMAIL);
        pocetObj=intent.getIntExtra(MainActivity.RESERVATIONS_NUMBER, 0);
        retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //setUpDatabase();
        /*Date d=new Date();
        Reservation r =new Reservation("dfg Veronike",d,"","Milpoš","1",2);
        Reservation r2 =new Reservation("asdwq Veronike",d,"","Milpoš","2",1);
        Reservation r3 =new Reservation("Pwed Veronike",d,"","Milpoš","3",3);
        Reservation r4 =new Reservation("Asadwql Veronike",d,"","Milpoš","4",4);
        Reservation r5 =new Reservation("AsPOTVRDENA Veronike",d,"","Milpoš","5",2);
        myReservations.add(r);
        myReservations.add(r2);
        myReservations.add(r3);
        myReservations.add(r4);
        myReservations.add(r5);*/

        noRes = (TextView)findViewById(R.id.noReservations);

        //System.out.println(myReservations);
        adapter = new ListViewAdapter(this, R.layout.item_reservation, myReservations);
        listView= (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent1 = new Intent(ReservationsActivity.this, DetailActivity.class);
                intent1.putExtra("Token", myReservations.get(position).getToken());
                intent1.putExtra("Request", myReservations.get(position).getProblem());
                intent1.putExtra("State", myReservations.get(position).getState());
                DateFormat df=new SimpleDateFormat("dd.MM.yyyy");
                DateFormat df2=new SimpleDateFormat("HH:mm");
                String date = df.format(myReservations.get(position).getTime());
                String time = df2.format(myReservations.get(position).getTime());
                intent1.putExtra("Date", date);
                intent1.putExtra("Time", time);
                intent1.putExtra("Office", myReservations.get(position).getOffice());
                intent1.putExtra("Address", myReservations.get(position).getLocation());
                startActivity(intent1);
            }
        });
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        Button newReservation = (Button)findViewById(R.id.fab);
        newReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReservationsActivity.this, MainActivity.class);
                intent.putExtra(MainActivity.USER_EMAIL, email);
                startActivity(intent);
            }
        });

        getReservationsFromServer();
        noRes.setVisibility(View.GONE);
        if(myReservations.isEmpty()){
            noRes.setVisibility(View.VISIBLE);
        }
        else noRes.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(myReservations.isEmpty()){
            noRes.setVisibility(View.VISIBLE);
        }
        else noRes.setVisibility(View.GONE);
    }

    private Date stringToDate(String aDate, String aFormat) {

        if(aDate==null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate, pos);
        return stringDate;

    }


    private void getReservationsFromServer() {
        // prepare call in Retrofit 2.0
        RestApi myApi = retrofit.create(RestApi.class);
        Call<List<Urad>> call = myApi.loadOffices();
        call.enqueue(new Callback<List<Urad>>() {
            @Override
            public void onResponse(Response<List<Urad>> response, Retrofit retrofit) {
                for (int i = 0; i < response.body().size(); i++) {
                    for(int j = 0; j < response.body().get(i).getObjednavkas().size(); j++) {
                        String token = response.body().get(i).getObjednavkas().get(j).getToken();
                        String dateTime = response.body().get(i).getObjednavkas().get(j).getDateTime();
                        //System.out.println("DATETIME: "+dateTime);
                        Date date = stringToDate(dateTime, "yyyy-MM-dd'T'HH:mm:ss");
                        String adresaUradu = response.body().get(i).getAdresa();
                        String urad = response.body().get(i).getNazov();
                        String mail = response.body().get(i).getObjednavkas().get(j).getEmail();

                        if(mail.equals(email)) {
                            System.out.println("Token: "+token);
                            mydatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
                            Cursor res2 =  mydatabase.rawQuery("SELECT * FROM "+MainActivity.RESERVATIONS_TABLE+" WHERE "+MainActivity.TOKEN+" = '" + token + "'", null);
                            System.out.println(res2.getCount());
                            res2.moveToNext();
                            String resId = res2.getString(res2.getColumnIndex(MainActivity.RESERVATION_ID)); //res3.getString(0);
                            System.out.println("RES ID "+resId);
                            Cursor res3 =  mydatabase.rawQuery("SELECT "+MainActivity.REQUEST+" FROM "+MainActivity.REQUESTS_TABLE+" WHERE "+MainActivity.RESERVATION_ID+" = "+resId, null);
                            res3.moveToNext();
                            String request = res3.getString(res3.getColumnIndex(MainActivity.REQUEST)); //res3.getString(0);
                            System.out.println("REQUEST "+request);

                            myReservations.add(new Reservation(request, date, urad, adresaUradu, token, 2));
                        }
                        //System.out.println(myReservations);
                        adapter = new ListViewAdapter(ReservationsActivity.this, R.layout.item_reservation, myReservations);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                    Log.d("Objednavok ", "" + response.body().get(i).getObjednavkas().size());
                }

            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(ReservationsActivity.this, "Chyba v internetovom pripojení. Údaje boli načítané z lokálnej databázy.", Toast.LENGTH_LONG).show();
                loadFromDbs();
            }
        });
    }

    private void loadFromDbs() {
        mydatabase = openOrCreateDatabase(DATABASE_NAME,MODE_PRIVATE,null);
        //mydatabase.execSQL("DROP TABLE IF EXISTS "+RESERVATIONS_TABLE);
        /*mydatabase.execSQL("CREATE TABLE IF NOT EXISTS "+RESERVATIONS_TABLE+"("+TOKEN+" VARCHAR,"+DATETIME+
                " VARCHAR,"+URAD_ID+" INTEGER,"+STATE+" INTEGER);");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // prepare call in Retrofit 2.0
        myApi = retrofit.create(RestApi.class);
        Call<List<Urad>> call = myApi.loadOffices();
        call.enqueue(new Callback<List<Urad>>() {
            @Override
            public void onResponse(Response<List<Urad>> response, Retrofit retrofit) {
                for (int i = 0; i < response.body().size(); i++) {
                    for(int j = 0; j < response.body().get(i).getObjednavkas().size(); j++) {
                        String token = response.body().get(i).getObjednavkas().get(j).getToken();
                        String dateTime = response.body().get(i).getObjednavkas().get(j).getDateTime();
                        int id = Integer.parseInt(response.body().get(i).getUradId());
                        String mail = response.body().get(i).getObjednavkas().get(j).getEmail();
                        if(mail.equals(email)) {
                            //mydatabase.execSQL("INSERT INTO "+RESERVATIONS_TABLE+" VALUES('"+token+"','"+dateTime+"','"+id+"','2');");
                            //System.out.println("INSERT "+token);
                        }
                    }
                }
                ArrayList<String> array_list = new ArrayList<>();*/
                Cursor res =  mydatabase.rawQuery( "SELECT * FROM "+MainActivity.RESERVATIONS_TABLE, null);
                System.out.println("LoadFromDBS - pocet OBJ "+res.getCount());

                res.moveToFirst();

                while(res.isAfterLast() == false){
                    //array_list.add(res.getString(res.getColumnIndex(TOKEN)));
                    String token = res.getString(res.getColumnIndex(MainActivity.TOKEN));
                    int resId = res.getInt(res.getColumnIndex(MainActivity.RESERVATION_ID));
                    Cursor res3 =  mydatabase.rawQuery("SELECT "+MainActivity.REQUEST+" FROM "+MainActivity.REQUESTS_TABLE+" WHERE "+MainActivity.RESERVATION_ID+" = "+resId, null);
                    res3.moveToNext();
                    String request = res3.getString(res3.getColumnIndex(MainActivity.REQUEST)); //res3.getString(0);
                    System.out.println("REQUEST "+request);
                    String dateTime = res.getString(res.getColumnIndex(MainActivity.DATETIME));
                    Date date = stringToDate(dateTime, "yyyy-MM-dd'T'HH:mm");
                    System.out.println(dateTime+" - "+date);
                    int state = res.getInt(res.getColumnIndex(MainActivity.STATE));
                    int id = res.getInt(res.getColumnIndex(MainActivity.URAD_ID));
                    //System.out.println(token+", "+request+", "+id+", "+state);
                    Cursor res2 =  mydatabase.rawQuery(
                            "SELECT * FROM "+MainActivity.OFFICES_TABLE+" WHERE "+MainActivity.URAD_ID+" = "+id, null);
                    res2.moveToFirst();
                    String nazov = res2.getString(res2.getColumnIndex(MainActivity.URAD_NAME));
                    String adresa = res2.getString(res2.getColumnIndex(MainActivity.ADDRESS));
                    //System.out.println(nazov+", "+adresa);
                    myReservations.add(new Reservation(request, date, nazov, adresa, token, state));
                    res.moveToNext();
                }

            /*}

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(ReservationsActivity.this, "Synchronizácia zlyhala. Skontrolujte internetové pripojenie.", Toast.LENGTH_LONG).show();
                System.out.println(t.getLocalizedMessage());
            }
        });*/
        adapter = new ListViewAdapter(ReservationsActivity.this, R.layout.item_reservation, myReservations);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }



}