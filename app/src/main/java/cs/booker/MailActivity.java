package cs.booker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by PaFi on 30. 3. 2016.
 */
public class MailActivity extends Activity {
    String mail;
    String request;
String predMail="Na ";
    String predreq=" Vám bol zaslaný aktivačný odkaz, po ktorého potvrdení Vašu požiadavku - ";
    String poreq=" - zaradíme do systému.";
    TextView txt;
    Button b;
    int pocetObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);
        Intent intent=getIntent();
         mail=intent.getStringExtra(MainActivity.USER_EMAIL);
        request=intent.getStringExtra(MainActivity.USER_REQUEST);
        pocetObj=intent.getIntExtra(MainActivity.RESERVATIONS_NUMBER, 0);
        txt= (TextView) findViewById(R.id.textMailAct);
        txt.setText(predMail+ mail +predreq+ request+poreq);

        b= (Button) findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRez();
            }
        });
    }

    private void startRez() {
        Intent intent = new Intent(this, ReservationsActivity.class);
        intent.putExtra(MainActivity.USER_REQUEST, request);
        intent.putExtra(MainActivity.USER_EMAIL, mail);
        intent.putExtra(MainActivity.RESERVATIONS_NUMBER, pocetObj);
        startActivity(intent);
    }
}


