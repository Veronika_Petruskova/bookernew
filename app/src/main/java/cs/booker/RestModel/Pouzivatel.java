package cs.booker.RestModel;

/**
 * Created by Veronika on 6. 5. 2016.
 */
public class Pouzivatel {

    private String ContactId;
    private String Address;
    private String City;
    private String Email;
    private String Name;
    private String Self;
    private String State;
    private String Twitter;
    private String Zip;

    public Pouzivatel() {}

    public String getContactId() {
        return ContactId;
    }

    public String getAddress() {
        return Address;
    }

    public String getCity() {
        return City;
    }

    public String getEmail() {
        return Email;
    }

    public String getName() {
        return Name;
    }

    public String getSelf() {
        return Self;
    }

    public String getState() {
        return State;
    }

    public String getTwitter() {
        return Twitter;
    }

    public String getZip() {
        return Zip;
    }
}
