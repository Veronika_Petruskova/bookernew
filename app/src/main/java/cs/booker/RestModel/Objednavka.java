package cs.booker.RestModel;

/**
 * Created by Veronika on 7. 5. 2016.
 */
public class Objednavka {
    private String ObjednavkaId;
    private String Token;
    private String Datum;
    private String DateTime;
    private int UradId;
    private String Self;
    private String Email;

    public Objednavka(String email, String dateTime, int uradId) {
        this.DateTime = dateTime;
        this.Email = email;
        this.UradId = uradId;
    }

    public String getObjednavkaId() {
        return ObjednavkaId;
    }

    public String getToken() {
        return Token;
    }

    public String getDatum() {
        return Datum;
    }

    public String getDateTime() {
        return DateTime;
    }

    public int getUradId() {
        return UradId;
    }

    public String getSelf() {
        return Self;
    }

    public String getEmail() {
        return Email;
    }
}
